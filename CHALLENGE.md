# Frontend Developer Coding Challenge

### Your challenge is to build an application to list the top 10 questions on Stack Overflow.

## Requirements:
* Build a responsive web interface based on the included desktop designs - Please use your intuition as to how it should adapt for smaller screens. (For accuracy, we recommend using the Figma version available here: https://www.figma.com/file/fsFs4MJp0AK8kiPj2RKpW60f/FED-Project-Layout-TEST?node-id=1%3A2)
* Use whatever technology you are comfortable with to develop a web frontend.
* Use the following Stack Overflow API method to source the question data: https://api.stackexchange.com/2.2/questions?pagesize=10&order=desc&sort=votes&site=stackoverflow
* Send your solution through within 48 hours, but please don't spend any more than **4 hours** on it, even if you don't think it's finished or ready. We're more interested in your process than seeing a _final_ product. (We recommend spending at least **1 hour on styling**)
* Include a README.md that has instructions on how to run your app as well as any design notes (what areas you focused on getting done, what you learnt, what you would do with more time).
* Submit a pull request at the end of the time period and let us know via email.

## Your code challenge will be reviewed against the following criteria:
* Styling and interactive aspects (please hand code your HTML & CSS/SASS/LESS to show off your skills here... No Bootstrap.)
* Functionality
* Creativity
* Code readability and clarity
* Test quality (if you have time)

Note: The point of this challenge is to demonstrate your own code, please do not copy existing solutions from the web or other sources (you can use an app starter, but please note in your readme what you've used). It's best to focus on the areas of this challenge that highlight your strengths as a web developer!