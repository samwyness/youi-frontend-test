import React from 'react';

import 'styles/components/hero.scss';

import stack10 from 'assets/stack-10.svg';
import arrowDown from 'assets/arrow-down.svg';

const scrollToContent = () => {
  const content = document.querySelector('main');
  content &&
    content.scrollIntoView({
      behavior: 'smooth'
    });
};

const Hero = () => {
  return (
    <div className="youi-hero">
      <div className="flex-row-wrap justify-center">
        <div className="youi-hero__image">
          <img src={stack10} alt="stackoverflow number 10" />
        </div>

        <div className="youi-hero__content flex-column">
          <h1 className="title">Most viewed questions of all time.</h1>
          <button className="scroll-btn" onClick={scrollToContent}>
            <img src={arrowDown} alt="scroll to content" />
          </button>
        </div>
      </div>

      <p className="youi-hero__info text-center">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
      </p>
    </div>
  );
};

export default Hero;
