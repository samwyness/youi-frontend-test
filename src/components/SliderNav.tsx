import React from 'react';

import 'styles/components/slider-nav.scss';

const SliderNav = () => {
  return (
    <div className="youi-header__slider-nav">
      <span className="ellipse active"></span>
      <span className="ellipse"></span>
      <span className="ellipse"></span>
    </div>
  );
};

export default SliderNav;
