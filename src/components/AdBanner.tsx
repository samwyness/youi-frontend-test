import React from 'react';

import '../styles/components/ad-banner.scss';

type AdBannerProps = {
  image: {
    src: string;
    alt: string;
  };
  tagline: string;
};

const AdBanner = ({ image, tagline }: AdBannerProps) => {
  return (
    <section>
      <div className="youi-ad-banner flex-col flex-center">
        <img src={image.src} alt={image.alt} />
        <span>{tagline}</span>
      </div>
    </section>
  );
};

export default AdBanner;
