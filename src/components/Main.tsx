import React, { useState, useEffect } from 'react';

import 'styles/components/main.scss';

import { fetchQuestions } from 'utils/stackService';

import QuestionCard from 'components/QuestionCard';
import AdBanner from 'components/AdBanner';

const Main = () => {
  const [questions, setQuestions] = useState<Questions>([]);

  useEffect(() => {
    questions.length === 0 &&
      fetchQuestions(10).then(response => setQuestions(response.data.items));
  });

  return (
    <main>
      <section>
        <div className="container">
          <ul className="list-style-none">
            {questions.slice(0, 5).map((question, index) => (
              <li key={index}>
                <QuestionCard number={index + 1} data={question} />
              </li>
            ))}
          </ul>
        </div>
      </section>

      <AdBanner image={{ src: '', alt: '' }} tagline="Sponsored by Youi" />

      <section>
        <div className="container">
          <ul className="list-style-none">
            {questions.slice(5).map((question, index) => (
              <li key={index}>
                <QuestionCard number={6 + index} data={question} />
              </li>
            ))}
          </ul>
        </div>
      </section>
    </main>
  );
};

export default Main;
