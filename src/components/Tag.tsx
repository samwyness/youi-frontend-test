import React from 'react';

import '../styles/components/tag.scss';

type TagProps = {
  text: string;
};

const Tag = ({ text }: TagProps) => {
  return <div className="youi-tag">{text}</div>;
};

export default Tag;
