import React from 'react';

import 'styles/components/question-card.scss';

import { twoDigit, formatNumber } from 'utils/helpers';

import AuthorBadge from 'components/AuthorBadge';
import Tag from 'components/Tag';

type QuestionCardProps = {
  number: number;
  data: IQuestion;
};

const QuestionCard = ({ number, data }: QuestionCardProps) => {
  return (
    <div className="youi-question-card">
      <div className="youi-question-card__number">#{twoDigit(number)}</div>

      <div className="youi-question-card__content">
        <div className="youi-question-card__title">
          <a href={data.link} title={`Go to question '${data.title}'`}>
            <strong dangerouslySetInnerHTML={{ __html: data.title }}></strong>
          </a>
        </div>

        <AuthorBadge author={data.owner} />

        <div className="youi-question-card__tags flex-row-wrap">
          {data.tags.map(tag => (
            <Tag text={tag} />
          ))}
        </div>
      </div>

      <div className="youi-question-card__stats">
        <div className="flex-row">
          <div className="youi-question-card__stats__answers flex-col text-center">
            <span>{data.answer_count}</span>
            ANSWERS
          </div>
          <div className="youi-question-card__stats__views flex-col text-center">
            <span>{formatNumber(data.view_count)}</span>
            VIEWS
          </div>
        </div>
        <a className="youi-question-card__stats__link" href={data.link}>
          VIEW
        </a>
      </div>
    </div>
  );
};

export default QuestionCard;
