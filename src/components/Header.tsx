import React from 'react';

import 'styles/components/header.scss';

import stackLogo from 'assets/stack-logo.svg';

import Hero from './Hero';
import SliderNav from './SliderNav';

const Header = () => {
  return (
    <header className="youi-header">
      <div className="container flex-col flex-center">
        <div className="youi-header__logo">
          <img src={stackLogo} alt="stackoverflow logo" />
        </div>

        <Hero />

        <SliderNav />
      </div>
    </header>
  );
};

export default Header;
