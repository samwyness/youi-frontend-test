import React from 'react';

import 'styles/components/author-badge.scss';

import clappingHands from 'assets/clapping-hands.svg';
import Tag from './Tag';

type AuthorBadgeProps = {
  author: IQuestion['owner'];
};

const AuthorBadge = ({ author }: AuthorBadgeProps) => {
  return (
    <div className="youi-author-badge flex-row align-center">
      <Tag text="OWNER" />

      <img
        className="youi-author-badge__avatar"
        src={author.profile_image}
        alt={`Author: ${author.display_name}`}
      />

      <span className="youi-author-badge__name">{author.display_name}</span>

      <span className="youi-author-badge__rep flex-row align-center">
        <img src={clappingHands} alt="Reputation icon" />
        <div className="flex-col text-center">
          <span>{author.reputation}</span>
          REPUTATION
        </div>
      </span>
    </div>
  );
};

export default AuthorBadge;
