import stackApi from './stackApi';

/**
 * Gets an array of stackoverflow questions, sorted by number of votes, from the
 * stack exchange api.
 *
 * @param {number} count - The number of results to return in the request
 */
export const fetchQuestions = (count = 1) => {
  const queryParams = new URLSearchParams({
    pagesize: count.toString(),
    order: 'desc',
    sort: 'votes',
    site: 'stackoverflow'
  }).toString();

  return stackApi
    .get(`/questions?${queryParams}`)
    .then(response => {
      return response;
    })
    .catch(error => {
      throw new Error(error);
    });
};
