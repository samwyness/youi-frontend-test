/**
 * Formats a number by prefixing it with a '0' if the number is below 10
 *
 * @param {number} number - The number to format
 * @return {string}
 */
export const twoDigit = (number: number) => {
  return number < 10 ? ('0' + number).slice(-2) : number.toString();
};

/**
 * Returns a formatted string where the number is trimmed and an abbreviation
 * representing the numbers value is appended to the results
 *
 * @param {number} number - The number to format
 * @return {string}
 */
export const formatNumber = (number: number) => {
  let abbr = '';
  let floatNum = 1;

  if (Math.abs(Number(number)) >= 1.0e6) {
    abbr = 'm';
    floatNum = 1.0e6;
  } else if (Math.abs(Number(number)) >= 1.0e3) {
    abbr = 'k';
    floatNum = 1.0e3;
  }

  return `${(Math.abs(Number(number)) / floatNum).toFixed(1)}${abbr}`;
};
