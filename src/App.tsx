import React from 'react';
import Helmet from 'react-helmet';

import 'styles/app.scss';

import AppConstants from 'utils/constants';

import Header from 'components/Header';
import Main from 'components/Main';
import Footer from 'components/Footer';

function App() {
  return (
    <div className="youi-app">
      <Helmet>
        <title>{AppConstants.APP_TITLE}</title>
      </Helmet>

      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
