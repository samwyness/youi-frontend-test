# Youi Frontend Test

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting Started

Install the project by running the command `yarn install` in the project directory

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

---

### Design decisions:

- Component based architecture, separating the design into individual functional or logical components
- Sass + BEM methodology for styling components
- Typescript implementation for static verification of code
- Api service wrapper for handling XHR's

### Area of focus:

Responsive mobile friendly UI

- By building components as individual parts of the design, I was able to
  quickly and efficiently adapt the layout to suit different screen sizes.
- Utilizing the flexbox css module allowed for components to easily adapt to
  different screen sizes.

### Thoughts and future improvements:

Overall I spent close to 5 hours on this test as I wanted to finish as much of
the task as possible to help show a clear representation of my technical
knowledge. Based on the given design and tasks included, I feel I completed
roughly 95% of test within the 5 hours.

However with more time I would have liked to include more improvements and
features to the app.

- Create a loading component to display while fetching data from the API
- Add slider functionality for the above the fold content (Hero component)
- Create a Author Card component for displaying more details about the
  questions author

Useful tests for this application would include:

- Testing api requests, sending/recieving data
- Component rendering based on logic and data fetching requirements (e.g. Does the load more button become hidden when there a no more results for the given query)
